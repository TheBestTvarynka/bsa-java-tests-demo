package com.example.demo.application;

import com.example.demo.dto.ToDoSaveRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class DemoApplicationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void whenUpsertWithWrongId_thenReturnValidResponseWithErrorMessage() throws Exception {
        Long id = 5L;
        ToDoSaveRequest saveRequest = new ToDoSaveRequest();
        saveRequest.id = id;
        saveRequest.text = "example of the text";

        ObjectMapper objectMapper = new ObjectMapper();
        this.mockMvc.perform(post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(saveRequest))
                .characterEncoding("utf-8"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(mvcResult -> assertEquals(
                        mvcResult.getResponse().getContentAsString(), String.format("Can not find todo with id %d", id))
                );
    }

    @Test
    void whenUpsertWithoutId_thenReturnObjectWithId() throws Exception {
        String testText = "example of the text";
        ToDoSaveRequest saveRequest = new ToDoSaveRequest();
        saveRequest.id = null;
        saveRequest.text = testText;

        ObjectMapper objectMapper = new ObjectMapper();
        this.mockMvc.perform(post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(saveRequest))
                .characterEncoding("utf-8"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("$.text").value(testText))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1L)) // 1L because database is empty
                .andExpect(jsonPath("$.completedAt").doesNotExist());

    }

}
