package com.example.demo.controller;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.exception.ToDoNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.util.Arrays;

import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.model.ToDoEntity;
import com.example.demo.service.ToDoService;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
class ToDoControllerIT {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ToDoService toDoService;

	@Test
	void whenGetAll_thenReturnValidResponse() throws Exception {
		String testText = "My to do text";
		Long testId = 1L;
		when(toDoService.getAll()).thenReturn(
			Arrays.asList(
				ToDoEntityToResponseMapper.map(new ToDoEntity(testId, testText))
			)
		);
		
		this.mockMvc
			.perform(get("/todos"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$").isArray())
			.andExpect(jsonPath("$", hasSize(1)))
			.andExpect(jsonPath("$[0].text").value(testText))
			.andExpect(jsonPath("$[0].id").isNumber())
			.andExpect(jsonPath("$[0].id").value(testId))
			.andExpect(jsonPath("$[0].completedAt").doesNotExist());
	}

	@Test
	void whenGetOne_thenReturnValidObject() throws Exception {
		String testText = "apple";
		Long id = 1L;
		when(toDoService.getOne(id)).thenReturn(
				ToDoEntityToResponseMapper.map(new ToDoEntity(id, testText))
		);

		this.mockMvc
				.perform(get("/todos/1"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").isMap())
				.andExpect(jsonPath("$.text").value(testText))
				.andExpect(jsonPath("$.id").isNumber())
				.andExpect(jsonPath("$.id").value(id))
				.andExpect(jsonPath("$.completedAt").doesNotExist());
	}

	@Test
	void whenUpsertNewToDo_thenReturnToDoObjectWithId() throws Exception {
		String testText = "apple";
		Long id = 1L;
		ToDoSaveRequest saveRequest = new ToDoSaveRequest();
		saveRequest.text = testText;
		saveRequest.id = null;

		ToDoResponse response = new ToDoResponse();
		response.text = testText;
		response.id = id;
		when(toDoService.upsert(ArgumentMatchers.any(ToDoSaveRequest.class))).thenReturn(response);

		ObjectMapper objectMapper = new ObjectMapper();
		this.mockMvc
				.perform(post("/todos")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(saveRequest))
						.characterEncoding("utf-8"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").isMap())
				.andExpect(jsonPath("$.text").value(testText))
				.andExpect(jsonPath("$.id").isNumber())
				.andExpect(jsonPath("$.id").value(id))
				.andExpect(jsonPath("$.completedAt").doesNotExist());
	}

	@Test
	void whenUpsertInvalidToDo_thenReturnPlainTextWithErrorMessage() throws Exception {
		String testText = "apple";
		Long id = 1L;
		ToDoSaveRequest saveRequest = new ToDoSaveRequest();
		saveRequest.text = testText;
		saveRequest.id = null;

		ToDoResponse response = new ToDoResponse();
		response.text = testText;
		response.id = id;
		when(toDoService.upsert(ArgumentMatchers.any(ToDoSaveRequest.class))).thenThrow(new ToDoNotFoundException(id));

		ObjectMapper objectMapper = new ObjectMapper();
		this.mockMvc
				.perform(post("/todos")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(saveRequest))
						.characterEncoding("utf-8"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("text/plain;charset=UTF-8"))
				.andExpect(mvcResult -> assertEquals(
						mvcResult.getResponse().getContentAsString(), String.format("Can not find todo with id %d", id))
				);
	}

}
