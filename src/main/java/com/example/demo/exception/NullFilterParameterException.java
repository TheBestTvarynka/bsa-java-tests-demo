package com.example.demo.exception;

public class NullFilterParameterException extends IllegalArgumentException {

    public NullFilterParameterException() {
        super("Error: filter argument must be not null!");
    }

    public NullFilterParameterException(String parameter) {
        super(String.format("Error: %s filter argument must be not null!", parameter));
    }

}
